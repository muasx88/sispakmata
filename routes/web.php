<?php

Route::get('/', function () {
    return view('welcome');
});
Route::get('/admin/home', 'Admin\HomeController@index');
Route::get('/admin/gejala/', 'Admin\GejalaController@index');
Route::get('/admin/penyakit/', 'Admin\PenyakitController@index');