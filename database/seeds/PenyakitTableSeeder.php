<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class PenyakitTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
    	for ($i=0; $i < 50 ; $i++) { 
	        DB::table('penyakit')->insert([
	            'kd_penyakit' => $faker->currencyCode ,
	            'nm_penyakit' => $faker->streetName
	        ]);
    		
    	}
    }
}
