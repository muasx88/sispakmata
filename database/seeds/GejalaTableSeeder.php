<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class GejalaTableSeeder extends Seeder
{
    public function run()
    {
    	$faker = Faker::create();
    	for ($i=0; $i < 50 ; $i++) { 
	        DB::table('gejala')->insert([
	            'kd_gejala' => $faker->currencyCode ,
	            'nm_gejala' => $faker->streetName
	        ]);
    		
    	}

    }
}
